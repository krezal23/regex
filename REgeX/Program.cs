﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace REgeX
{
    class Program
    {
        static void Main(string[] args)
        {
            var program = new Program();
            program.Run();
        }
        private void Run()
        {
            
            var stringFromFile = File.ReadAllText("../../../../domains.html");
            var dicionaryOfMatches = new Dictionary<string, int>();
            GetDomainNames(stringFromFile, dicionaryOfMatches);

            static void WriteDictToFile(Dictionary<string, int> dicionaryOfMatches, string path)
            {
                using var fileWriter = new StreamWriter(path);
                {

                    foreach (KeyValuePair<string, int> kvPair in dicionaryOfMatches)
                    {
                        fileWriter.WriteLine("{0}: {1}", kvPair.Key, kvPair.Value);
                    }
                    fileWriter.Close();
                };

            }

            static void GetDomainNames(string stringFromFile, Dictionary<string, int> dicionaryOfMatches)
            {
                if (File.Exists("../../../../domains.html"))
                {
                    MatchCollection matches = FindMaches(stringFromFile);
                    foreach (Match match in matches)
                    {
                        if (dicionaryOfMatches.ContainsKey(match.Value))
                        {
                            dicionaryOfMatches[match.Value]++;
                        }
                        else
                            dicionaryOfMatches.Add(match.Value, 1);

                        WriteDictToFile(dicionaryOfMatches, "../../../../domains.txt");
                    }
                }
                else
                    Console.WriteLine("no such file");
            }
        }

        private static MatchCollection FindMaches(string stringFromFile)
        {
            Regex rx = new Regex(@"(?:[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?\.)+[a-z0-9][a-z0-9-]{0,61}[a-z0-9]", RegexOptions.IgnoreCase);
            MatchCollection matches = rx.Matches(stringFromFile);
            Console.WriteLine("matches found in: " + matches.Count);
            return matches;
        }
    }
}
